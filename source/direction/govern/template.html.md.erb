---
layout: sec_direction
title: Product Stage Direction - Govern
description: "GitLab Govern helps users manage security vulnerabilities, policies, and compliance across their organization. Learn more!"
canonical_path: "/direction/govern/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

<p align="center">
    <font size="+2">
        <b>Organization-wide security vulnerability, policy, and compliance management</b>
    </font>
</p>

<%= partial("direction/govern/templates/overview") %>

<%= devops_diagram(["Govern"]) %>

## Stage Overview

The Govern stage provides the capabilities necessary to meet security and compliance requirements for organizations at any scale, from one project to tens of thousands of projects. This includes the ability to manage policies centrally, at scale, and have them apply to projects across the organization.

### Groups

The Govern Stage is made up of three groups:

* Compliance - Provide users with the tools and features necessary to manage their compliance programs.
* Security Policies - Apply policies to enforce scans and to require security approvals when vulnerabilities are found.
* Threat Insights - Holistically view, manage, and reduce potential risks across the entire DevSecOps lifecycle.

### Resourcing and Investment

The existing team members for the Govern Stage can be found in the links below:

* [Development](https://about.gitlab.com/company/team/?department=govern-section)
* [User Experience](https://about.gitlab.com/company/team/?department=govern-ux-team)
* [Product Management](https://about.gitlab.com/company/team/?department=govern-pm-team)
* [Quality Engineering](https://about.gitlab.com/company/team/?department=sec-datascience-qe-team)

## 3 Year Stage Themes

<%= partial("direction/govern/templates/themes") %>

## 3 Year Strategy

Building on those themes, some specific capabilities that we envision developing over the next 3 years include the following:

**Compliance**

1. Even more comprehensive visibility and security related to access tokens, keys, and credentials.
1. Added support to view how projects adhere to an expanded list of compliance and regulatory requirements.

**Security Policies**

1. A unified policy management experience for all security policies, spanning across GitLab's stages, including support for policies to protect against pipeline abuse and policies to protect against insider threats.
1. The ability to proactively notify users about newly discovered vulnerabilities in production or in the default branch.
1. Improved policy change management tools, including visibility into the scope/impact of policy changes, as well as improved visibility into the history and approvals for changes made to policies.

**Threat Insights**

1. Enhanced visibility provided by proactive alerts, configurable dashboards, and customizable reports will put the right information in the right form at the right time.
1. Custom risk profiles to let your organization determine the business criticality of projects and assets. This will enable risk-based decision making to maximize return on security efforts.

In addition to these areas specific to our individual groups, we also plan to expand our use of ML and AI across all of the Govern features.

## 1 Year Plan

Over the next 12 months, the Govern stage is focused on addressing critical needs for security and compliance teams.  Some of the key initiatives include the following:

1. Improved management of compliance framework labels to control which labels are assigned to which projects.
1. Enabling users to view their dependencies across all of their projects at the group and sub-group levels.
1. Improved searching and filtering for both vulnerabilities and dependencies.
1. Additional types of security policies that can be enforced across all merge requests to ensure adherence with two-person approval compliance requirements.
1. A central dashboard at the project, sub-group, and group levels to enable compliance teams to quickly see whether or not the configuration of a project adheres to common compliance requirements.
1. Combining scan execution policies with compliance framework pipelines.
1. A unified way of viewing and searching across all audit events.
1. Added support for managing security (vulnerabilities, dependencies, policies, compliance) at the organizational level so self-managed instances can manage security centrally across all of their groups.

### What We're Not Doing

Although we will likely address many of these areas in the future (as described above in our [3 year strategy](/direction/govern/#3-year-strategy)), we are not planning to make progress on the following initiatives in the next 12 months:
* Attempting to build our own Security Information and Event Management (SIEM) system
* Building analytics or algorithms to auto-tune or auto-recommend policy improvements

## Competitive Landscape

This area is currently being re-evaluated

## Key Performance Metrics

The following metrics are used to evaluate the success of the Govern stage:

* Compliance **Group Monthly Active Users**: This is the total number of unique users viewing the Audit Events, Compliance Dashboard, or Credential Inventory pages in the last 28 days of the given month.
* Security Policies **Group Monthly Active Users**: This is the total number of users that have interacted with GitLab's Vulnerability Management in the last 28 days of the given month.
* Threat Insights **Group Monthly Active Users**: This is the total number of unique sessions viewing a security dashboard, pipeline security report, or expanding MR security report in the last 28 days of the given month.

Note: We do not yet have a single metric to track the success of the Govern stage as a whole.  This is being tracked in [this issue](https://gitlab.com/gitlab-data/product-analytics/-/issues/883).

## Target Audience
GitLab identifies who our DevSecOps application is built for utilizing the following categorization. We list our view of who we will support when in priority order.
* 🟩- Targeted with strong support
* 🟨- Targeted but incomplete support
* ⬜️- Not targeted but might find value

### Today
To capitalize on the opportunities listed above, the Govern Stage has features that make it useful to the following personas today.
1. 🟩  Developers / Development Teams
1. 🟩  Application Security Teams
1. 🟨️  Compliance Specialists / Manager
1. 🟨️  Legal Teams
1. 🟨  Infrastructure Security Teams

### Medium Term (1-2 years)
As we execute our [3 year strategy](#3-year-strategy), our medium term (1-2 year) goal is to provide a single DevSecOps application that enables SecOps to work collaboratively with DevOps and development to mitigate vulnerabilities in production environments.
1. 🟩  Developers / Development Teams
1. 🟩  Application Security Teams
1. 🟩  Compliance Specialists / Manager
1. 🟩  Legal Teams
1. 🟩  Infrastructure Security Teams

## Pricing

<%= partial("direction/govern/templates/pricing") %>

<%= partial("direction/categories", :locals => { :stageKey => "govern" }) %>

## Other Existing Top Level Features

Govern has several features that help teams provide necessary governance for their organizations.

#### Policy Management

GitLab's security policies page provides the flexibility of managing policies directly in code or in a streamlined UI editor.  As part of the long-term vision for the policy management experience, users will be able to view a complete history of all changes and easily revert to a previous version.  A two-step approval process can optionally be enforced for all policy changes.  Eventually the policy management UI will be extended to provide visibility into the performance overhead of each policy.  Suggestions into policy adjustments that might help either reduce false positives or increase overall security coverage will be provided in this section as well.

#### Security Approvals

Security approvals define when and how security teams get involved in the development workflow.  The vision for this area is to provide a highly granular level of approval definition functionality at the project, group, and workspace levels.  These capabilities will be tightly integrated with the Security Policy editor to allow for separation of duties for security teams.

### Security Dashboards

Security Dashboards, available at the [group](https://docs.gitlab.com/ee/user/application_security/security_dashboard/)
and [project](https://docs.gitlab.com/ee/user/application_security/security_dashboard/index.html) level, are the primary tool for Security Teams and Directors of Security. They can use those
dashboards to access the current security status of their applications and to start a
remediation process from there.

The dashboard also provides data and charts to summarize how the team is performing against their goals to maintain proper levels of security risk.

## Upcoming Releases

<%= direction["all"]["all"] %>

<%= partial("direction/other", :locals => { :stage => "govern" }) %>

<p align="center">
    <i><br />
    Last Reviewed: 2023-03-30<br />
    Last Updated: 2023-03-30
    </i>
</p>
